#!/usr/bin/env python

"""
TKInter-based utility to check WAN connection.

Uses threading code by Jacob Hallen, AB Strakt, Sweden. 2001-10-17

See:
http://code.activestate.com/recipes/82965-threads-tkinter-and-asynchronous-io/

Author:     Anthony Lopez-Vito, Another Cup of Coffee Limited
Contact:    http://anothercoffee.net
Version:    0.1
Date:       Sometime around 2019

License:
Unless otherwise specified by the project requirements, this code
has been released under the MIT License (MIT)
Copyright (c) 2018 Another Cup of Coffee Limited

See LICENSE.txt 

"""
import sys, os, getopt
try:
    from Tkinter import *
    #from ttk import *
    import ttk
except ImportError as e:
    print "This script requires Python2. Exiting."
    sys.exit(2)
import time
from datetime import datetime
import socket
import threading
import Queue
import logging, logging.handlers
#from acoclutils.utils import setup_logging
import tkMessageBox
from ScrolledText import *


_logger = logging.getLogger()
_debug = False

ONLINE, OFFLINE, CHECKING = range(0, 3)
GOOGLE_SELECTION = 1
CLOUDFLARE_SELECTION = 2
GOOGLE_HOST = "8.8.8.8"
GOOGLE_PORT = 53
CLOUDFLARE_HOST = "1.1.1.1"
CLOUDFLARE_PORT = 53
DEFAULT_HOST = GOOGLE_HOST
DEFAULT_PORT = GOOGLE_PORT
DEFAULT_SLEEP_SECONDS = 60
DEFAULT_SLEEP_SECONDS_DEBUG = 10
PROGRESS_BAR_MAXIMUM = 100


##################################################################
# Utility functions
##################################################################
def setup_logging(logger, settings):
    """Log output

        Sends log output to console or file,
        depending on error level
    """
    try:
        log_filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            settings['log_filename']
        )
        log_max_bytes = settings['log_max_bytes']
        log_backup_count = settings['log_backup_count']
    except KeyError as ex: 
        print "WARNING: Missing logfile setting {}. Using defaults.".format(ex)
        log_filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "log.txt"
        )
        log_max_bytes = 1048576 #1MB
        log_backup_count = 5

    logger.setLevel(logging.DEBUG)
    # Set up logging to file
    file_handler = logging.handlers.RotatingFileHandler(
        filename=log_filename,
        maxBytes=log_max_bytes,
        backupCount=log_backup_count
    )
    file_handler.setLevel(logging.ERROR)
    file_formatter = logging.Formatter(
        '%(asctime)s %(name)-15s %(levelname)-8s %(message)s',
        '%m-%d %H:%M'
    )
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    # Handler to write INFO messages or higher to sys.stderr
    #
    # SET LOGGING LEVEL HERE
    # * INFO
    # * DEBUG
    #
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO) # <--- Set logging level
    #console_handler.setLevel(logging.DEBUG) # <--- Set logging level
    console_formatter = logging.Formatter('%(levelname)-8s %(message)s')
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)
    logger.debug("---------------------------------")
    logger.debug(
        "Starting log for session %s",
        datetime.now().strftime("%d/%m/%Y %H:%M:%S:%f")
    )


def get_host_selection(selection):
    switcher = {
        GOOGLE_SELECTION: GOOGLE_HOST,
        CLOUDFLARE_SELECTION: CLOUDFLARE_HOST,
    }
    return switcher.get(selection, GOOGLE_HOST)


def get_port_selection(selection):
    switcher = {
        GOOGLE_SELECTION: GOOGLE_PORT,
        CLOUDFLARE_SELECTION: CLOUDFLARE_PORT,
    }
    return switcher.get(selection, GOOGLE_PORT)


def get_status_text(status):
    switcher = {
        ONLINE: "Online",
        OFFLINE: "Offline",
        CHECKING: "Checking",
    }
    return switcher.get(status, "Undefined")


def get_status_bg_color(status):
    switcher = {
        ONLINE: "LightGreen",
        OFFLINE: "Red",
        CHECKING: "Yellow",
    }
    return switcher.get(status, "White")


def is_online(host="8.8.8.8", port=53):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    status = False
    
    try:
        logging.debug("Trying {}:{}".format(host, port))
        socket.setdefaulttimeout(1)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        status = True
    except Exception as ex:
        template = "A {0} exception occured:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        logging.debug(message)
    
    return status


def format_connection_msg(is_connected):
    test_time = datetime.now()
    time_label = "{}".format(test_time.strftime("%Y-%m-%d %H:%M:%S"))

    if is_connected:
        status_message = "Connected as of {}".format(time_label)
    else:
        status_message = "Not connected as of {}".format(time_label)
    return status_message


##################################################################
# Classes
##################################################################
class MainWindow:
    def __init__(self, master, queue, start_connection_poll, stop_connection_poll):
        self.queue = queue
        self.master = master
        self.start_connection_poll = start_connection_poll
        self.stop_connection_poll = stop_connection_poll
        self.host = DEFAULT_HOST
        self.port = DEFAULT_PORT
        self.sleep_seconds = DEFAULT_SLEEP_SECONDS
        self.host_selection = IntVar()
        self.stop_online = IntVar()
        if is_online(self.host, self.port):
            self.connection_status = ONLINE
        else:
            self.connection_status = OFFLINE

        self.create_app_widgets()


    def create_app_widgets(self):
        self.master.option_add("*Font", "Verdana 10")
        self.master.option_add("*Label.Font", "Verdana 10")
        #self.master.option_add("*Button.Font", "Verdana 10")

        # Menu
        self.main_menu = Menu(self.master, tearoff=0)
        self.file_menu = Menu(self.main_menu, tearoff=0)
        self.file_menu.add_command(
            label="Start",
            underline=0,
            accelerator="Ctrl+S",
            command=self.start
        )
        self.file_menu.add_command(
            label="Stop",
            underline=3,
            accelerator="Ctrl+T",
            command=self.stop
        )
        self.file_menu.add_command(
            label="Quit",
            underline=0,
            accelerator="Ctrl+Q",
            command=self.master.quit
        )
        self.file_menu.add_command(label="About", command=self.show_help)
        self.main_menu.add_cascade(label="CheckWAN",menu=self.file_menu)
        self.master.config(menu=self.main_menu)
        
        # Keybindings
        self.master.bind('<Control-q>', self.kb_bind_quit)
        self.master.bind('<Control-s>', self.kb_bind_start)
        self.master.bind('<Control-t>', self.kb_bind_stop)


        self.master.title("WAN Connection Tester")

        # Title - master row 0
        self.title_label = Label(
            self.master,
            text="WAN connection",
            fg = "blue",
            font = "Verdana 12 bold"
        )
        self.title_label.grid(row=0, column=0, columnspan=3, sticky=W, padx=10, pady=15)

        # Status frame - master row 1
        self.status_frame = Frame(self.master)
        self.status_frame.grid(row=1, column=0, sticky=W, padx=(10, 5), pady=5)
        
        ## Status frame row 1
        self.connection_status_detail_label = Label(
            self.status_frame,
            text="Status"
        )
        self.connection_status_detail_label.grid(row=1, column=0, sticky=W, padx=5, pady=5)
        
        self.connection_status_indicator_text = Label(
            self.status_frame,
            text = get_status_text(self.connection_status),
            bg = get_status_bg_color(self.connection_status),
            font = "Verdana 10 bold"
        )
        self.connection_status_indicator_text.grid(
            row=1,
            column=1,
            #columnspan=2,
            sticky=W+E,
            padx=5,
            pady=5,
            ipadx=5,
            ipady=2
        )
        
        self.refresh_button = Button(
            self.status_frame,
            text="Refresh",
            command=lambda: self.update_connection_status(1)
        )
        self.refresh_button.grid(row=1, column=2, sticky=E, padx=10, pady=5)
        
        ## Status frame row 2
        self.connection_status_detail_label = Label(
            self.status_frame,
            text=format_connection_msg(is_online())
        )
        self.connection_status_detail_label.grid(
            row=2,
            column=0,
            columnspan=3,
            sticky=W,
            padx=5,
            pady=5)

        # Progress - master row 2
        self.progress = ttk.Progressbar(
            self.master,
            orient="horizontal",
            length=300,
            mode="determinate",
            #mode="indeterminate"
        )
        self.progress.grid(row=2, column=0, columnspan=3, sticky=W, padx=15, pady=(5,20))
        self.progress['maximum'] = PROGRESS_BAR_MAXIMUM


        # Control frame - master row 3
        self.control_frame = Frame(self.master) #, bg="Yellow")
        self.control_frame.grid(row=3, column=0, columnspan=3, sticky=W, padx=(10, 5), pady=5)

        ## Control frame row 0
        self.google_radio = Radiobutton(
            self.control_frame,
            text='Google (8.8.8.8:53)',
            variable=self.host_selection,
            value=1
        )
        self.cloudflare_radio = Radiobutton(
            self.control_frame,
            text='Cloudflare (1.1.1.1:53)',
            variable=self.host_selection,
            value=2
        )
        self.google_radio.grid(row=0, column=0, columnspan=3, sticky=W, padx=5, pady=(5,0))
        self.cloudflare_radio.grid(row=1, column=0, columnspan=3, sticky=W, padx=5, pady=(5,10))
        self.google_radio.select()

        ## Control frame row
        self.stop_online_check = Checkbutton(
            self.control_frame,
            text='Stop when online',
            onvalue=1,
            var=self.stop_online
        )
        self.stop_online_check.grid(row=2, column=0, columnspan=3, sticky=W, padx=5, pady=5)
        self.stop_online_check.select()
        
        ## Control frame row
        self.start_button = Button(self.control_frame, text="Start", command=self.start)
        self.start_button.grid(row=3, column=0, sticky=W, padx=5, pady=5)
        
        self.stop_button = Button(self.control_frame, text="Stop", state=DISABLED, command=self.stop)
        #self.stop_button.grid(row=0, column=1, columnspan=3, sticky=W, padx=(5, 10), pady=5)
        self.stop_button.grid(row=3, column=1, sticky=W, padx=(5, 10), pady=5)
        
        self.exit_button = Button(self.control_frame, text="Exit", command=self.master.quit)
        self.exit_button.grid(row=3, column=2, sticky=E, padx=10 , pady=5)        

        # Log frame - master row 8
        self.log_lable_frame = LabelFrame(self.master, text="Status log")
        self.log_lable_frame.grid(
            row=8,
            column=0,
            columnspan=3,
            sticky=W,
            padx=10,
            pady=(30,10)
        )

        #self.status_label = Label(self.log_lable_frame, text="Status log")
        #self.status_label.grid(row=0, column=0, sticky=W, padx=10, pady=(30,10))
        self.status_log_txt = ScrolledText(self.log_lable_frame, width=35, height=10)
        self.status_log_txt.grid(row=1, column=0, columnspan=3, sticky=W, padx=10, pady=10)
        
        self.clear_button = Button(self.log_lable_frame, text="Clear", command=self.clear_log)
        self.clear_button.grid(row=2, column=0, columnspan=3, sticky=E, padx=10, pady=10)


    """
    def read_bytes(self):
        self.bytes_counter += 500
        self.progress["value"] = 20
        if self.bytes_counter < self.maxbytes:
            # read more bytes after 100 ms
            self.master.after(100, read_bytes)
    """

    def process_queue(self):
        """
        Handle all the messages currently in the queue (if any).
        """
        while self.queue.qsize():
            try:
                msg = self.queue.get(0)
                self.status_log_txt.insert(INSERT,'{}\n'.format(msg))
                self.connection_status_detail_label.config(
                    text=format_connection_msg(is_online())
                )
            except Queue.Empty:
                logging.debug("Queue is empty")
                raise


    def start(self):
        self.host = get_host_selection(self.host_selection.get())
        self.port = get_port_selection(self.host_selection.get())
        self.update_connection_status()
        time_label = "{}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        self.status_log_txt.insert(INSERT,'Started {}\n'.format(time_label))
        self.status_log_txt.insert(INSERT,'Polling {}:{}\n'.format(self.host, self.port))
        self.toggle_start_off()
        self.progress.config(mode="indeterminate")
        self.progress.start((self.sleep_seconds*1000)/PROGRESS_BAR_MAXIMUM)
        self.start_connection_poll()


    def stop(self):
        time_label = "{}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        self.status_log_txt.insert(INSERT,'Stopped {}\n'.format(time_label))
        self.toggle_start_on()
        self.progress.stop()
        self.progress.config(mode="determinate")
        self.stop_connection_poll()


    def toggle_start_off(self):
        """Update UI when start button is clicked
        """
        self.stop_online_check.config(state="disabled")
        self.google_radio.config(state="disabled")
        self.cloudflare_radio.config(state="disabled")
        self.start_button.config(state="disabled")
        self.stop_button.config(state="normal")


    def toggle_start_on(self):
        """Update UI when stop button is clicked
        """
        self.stop_online_check.config(state="normal")
        self.start_button.config(state="normal")
        self.stop_button.config(state="disabled")
        self.google_radio.config(state="normal")
        self.cloudflare_radio.config(state="normal")        
        self.progress.stop()
        self.progress.config(mode="determinate")


    def clear_log(self):
        self.status_log_txt.delete(1.0,END)


    def kb_bind_quit(self, _event=None):
        """Binds the <Control-q> keyboard shortcut
        to the quit finction.
        """
        self.master.quit()


    def kb_bind_start(self, _event=None):
        """Binds the <Control-s> keyboard shortcut
        to the start finction.
        """
        self.start()


    def kb_bind_stop(self, _event=None):
        """Binds the <Control-t> keyboard shortcut
        to the start finction.
        """
        self.stop()

    """
    def update_connection_status(self):
        if is_online(self.host, self.port):
            self.connection_status = ONLINE
        else:
            self.connection_status = OFFLINE

        self.connection_status_indicator_text.config(
            text = get_status_text(self.connection_status),
            bg = get_status_bg_color(self.connection_status)
        )
    """
    
    def update_connection_status(self, counter=1):
        """Update the connection status label.
        
        Timer delay gives user feedback that something is happening.
        """
        status_text = "{} {}".format(get_status_text(CHECKING), counter)
        self.connection_status_indicator_text.config(
            text = get_status_text(CHECKING),
            bg = get_status_bg_color(CHECKING)
        )

        if counter > 0:
            self.master.after(1000, self.update_connection_status, counter-1)
        else:
            if is_online(self.host, self.port):
                self.connection_status = ONLINE
            else:
                self.connection_status = OFFLINE

            self.connection_status_indicator_text.config(
                text = get_status_text(self.connection_status),
                bg = get_status_bg_color(self.connection_status)
            )
            
            self.connection_status_detail_label.config(
                text=format_connection_msg(is_online())
            )


    def show_help(self):
            tkMessageBox.showinfo(
                'About CheckWAN',
                "Copyright (c) 2019 Anthony Lopez-Vito\n\nA simple app for checking WAN connection"
            )


class ThreadedClient:
    """
    Launch the main part of the GUI and the worker thread. periodicCall and
    endApplication could reside in the GUI part, but putting them here
    means that you have all the thread controls in a single place.
    """
    def __init__(self, master):
        """
        Start the GUI and the asynchronous threads. We are in the main
        (original) thread of the application, which will later be used by
        the GUI. We spawn a new thread for the worker.
        """
        self.master = master

        # Create the queue
        self.queue = Queue.Queue()

        # Set up the GUI part
        self.gui = MainWindow(
            master,
            self.queue,
            self.start_connection_poll,
            self.stop_connection_poll
        )
        self.active = False


    def check_queue_while_active(self):
        """
        Check the queue every 100 ms
        """
        logging.debug('periodic call running: {}'.format(self.active))
        
        if self.active:
            self.gui.process_queue()
            self.master.after(100, self.check_queue_while_active)
        #self.gui.process_queue()
        #self.master.after(100, self.check_queue_while_active)
        #else:
            # This is the brutal stop of the system. You may want to do
            # some cleanup before actually shutting it down.
            #import sys
            #sys.exit(1)
            #logging.debug("Quitting check_queue_while_active")
            


    def worker_thread_connection_poll(self, host, port, sleep_seconds):
        """
        This is where we handle the asynchronous I/O. For example, it may be
        a 'select()'.
        One important thing to remember is that the thread has to yield
        control.
        """
        logging.debug('Starting worker thread with {}:{} polling every {} sec'.format(host, port, sleep_seconds))
        while self.active:
            is_connected = is_online(host, port)
            msg = format_connection_msg(is_connected)
            self.queue.put(msg)
            if is_connected:
                if self.gui.stop_online.get() == 1:
                    msg = "Stopping {}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                    self.queue.put(msg)
                    #tkMessageBox.showinfo('Connection status', msg)
                    self.gui.update_connection_status()
                    self.active = False
                    self.gui.toggle_start_on()
                
            #else:
            self.gui.process_queue()
            time.sleep(sleep_seconds)


    def start_connection_poll(self):
        """
        Set up the thread to do asynchronous I/O
        """
        self.active = True
    	self.thread_connection_poll = threading.Thread(
            target=self.worker_thread_connection_poll,
            kwargs={
                'host':DEFAULT_HOST,
                'port':DEFAULT_PORT,
                'sleep_seconds': DEFAULT_SLEEP_SECONDS
            }
        )
        self.thread_connection_poll.setDaemon(1)
        self.thread_connection_poll.start()

        #Continue to check queue for as long as the thread is active
        self.check_queue_while_active()


    def stop_connection_poll(self):
        """Signal thread to become inactive
        """
        self.active = False
        


##################################################################
# These functions control the major tasks
##################################################################
def process_command_line_options(argv):
    """
    Edit to process any command-line options for this script.


    Args:
        argv: The command line options.    
    """
    global _debug
    global DEFAULT_SLEEP_SECONDS

    try:
        opts, args = getopt.getopt(
            argv,
            # Reminder: options that require an argument are followed by a colon
            # 'abc:d:'
            "dh",
            ["debug", "help"]
        )
    except getopt.GetoptError:
        print "Options error"
        sys.exit(2)

    action = None
    options = dict()
    # Get command line options and arguments
    if (len(opts) == 0) and (len(args) == 0):
        # Do this when no options or arguments are supplied
        pass
    else:
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print "No help available."
                sys.exit()
            elif opt in ("-d", "--debug"):
                print "*** Running script in debug mode ***"
                DEFAULT_SLEEP_SECONDS = DEFAULT_SLEEP_SECONDS_DEBUG
                _debug = True


def main(argv):
    """
    """
    process_command_line_options(argv)
    try:
        #settings_file = os.path.join(
        #    os.path.dirname(os.path.realpath(__file__)),
        #    "settings.yml"
        #)
        #settings = get_settings(settings_file)
        settings = {
            'log_filename': 'log.txt',
            'log_max_bytes': 1048576,
            'log_backup_count': 5
        }

        setup_logging(_logger, settings)
        _logger.info("Starting {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        
        root = Tk()
        client = ThreadedClient(root)
        root.mainloop()
        
        _logger.info("Exiting {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        
    except KeyboardInterrupt:
        print "\nScript shutdown before completion." 
        sys.exit(1)        
    sys.exit(0)
    


# Program entry point
if __name__ == "__main__":
    main(sys.argv[1:])

