# pyCheckWan

This is a Tkinter-based utility to check WAN connection.

I hacked it together to diagnose intermittent connection issues from a local network to the WAN. It periodically checks public DNS servers and logs connection attempts to a plain text file.

Author:     Anthony Lopez-Vito, Another Cup of Coffee Limited
Contact:    [https://anothercoffee.net](https://anothercoffee.net)
Version:    0.1
Date:       Sometime around 2019


## Notes

This is an old script that requires Python 2. It will need to be updated to work on modern environments.

### Acknowledgements
Uses threading code by Jacob Hallen, AB Strakt, Sweden. 2001-10-17

See:
[http://code.activestate.com/recipes/82965-threads-tkinter-and-asynchronous-io/](http://code.activestate.com/recipes/82965-threads-tkinter-and-asynchronous-io/)


## License
Written by Anthony Lopez-Vito of [Another Cup of Coffee Limited](https://anothercoffee.net).

Unless otherwise specified by the project requirements, this code
has been released under the MIT License (MIT)
Copyright (c) 2018 Another Cup of Coffee Limited

See LICENSE.txt 
